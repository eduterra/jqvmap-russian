// Your client scripts go there

(function($) {

  $.bigfoot.install('[data-map]', function() {
    var $this = $(this);
    var mapName = $this.attr('data-map');
    var _selectedRegions = $this.attr('data-selected-regions') || '';
    var selectedRegions = _selectedRegions.split(',');

    var enableZoom = this.hasAttribute('data-zoom');

    var colorRegion = $this.attr('data-color-region') || '#a3a3a3';
    var colorSelectedRegion = $this.attr('data-color-selected') || '#0e7bb6';

    var highlightedStates = {};

    selectedRegions.map(function(code) {
      highlightedStates[code] = colorSelectedRegion;
    });

    var width = $this.width();
    var height = width / 1.8;

    $this.css({
      width: width,
      height: height
    });

    $(window).resize(function() {
      var width = $this.parent().width();
      var height = width / 1.8;
      $this.css({
        width: width,
        height: height
      });
    });


    var map = $this.vectorMap({
      map: mapName,
      color: colorRegion,
      colors: highlightedStates,
      stroke: ['#f0f0f0', 1],
      backgroundColor: '#fff',
      hoverOpacity: 0.9,
      enableZoom: enableZoom,
      showTooltip: true,
      borderWidth: 2,
      borderColor: '#fff',
      shadow: {
        color: '#000',
        dx: 8,
        dy: 8,
        blur: Math.round(8 * 0.8)
      },

      onLabelShow: function(event, label, code) {
        if (selectedRegions.indexOf(code) != -1) {
          var el = document.getElementById(code);
          if (el) {
            label.html(el.innerHTML);
          } else label.html('');
        }
      },

      onRegionClick: function(element, code, region) {
        if (selectedRegions.indexOf(code) != -1) {
          var el = document.getElementById(code);
          if (el) {
            el.scrollIntoView();
          }
        }
      }

    });

  });

})(jQuery);


