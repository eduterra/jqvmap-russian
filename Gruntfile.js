'use strict';

module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    jade: {
      compile: {
        options: {
          data: {
            pkg: grunt.file.readJSON('package.json')
          }
        },
        files: [{
          src: ['**/*.jade', '!**/_*.jade'],
          dest: 'build/<%= pkg.version %>/examples',
          ext: '.html',
          cwd: 'example/',
          expand: true
        }]
      }
    },

    uglify: {
      options: {
        compress: true
      },
      jqvmap: {
        files: {
          'build/<%= pkg.version %>/jqvmap.min.js': [
            'js/jqvmap.js',
            'js/world_en.js',
            'js/russia_ru.js',
            'js/russia_en.js',
            'js/index.js'
          ]
        }
      }
    },

    stylus: {
      compile: {
        options: {
          compress: true
        },
        files: {
          'build/<%= pkg.version %>/jqvmap.css': 'css/jqvmap.styl'
        }
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-stylus');

  grunt.registerTask('default', ['uglify', 'stylus', 'jade']);

};
